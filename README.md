# HXF #
*Simplicity is a prerequisite for reliability*

*Edsger W. Dijkstra*

---

## INTRODUCTION ##

**HXF** is a software development framework that helps software developers be more productive by auto-generating communication code between application layers. The generated code follows a specific design approach that helps the developers create higher quality solutions by standardizing the way of communication between application layers.

HXF focuses on being a cross-platform solution by creating a design that supports source code generation for multiple platforms easily.

Code generation is limited to two aspects of tiered applications which are the communication between the business logic layer and the persistence layer and the communication between the presentation layer and the business logic layer.

HXF depends on a set of standards that were developed taking into consideration simplicity and functionality. The framework aims at providing an efficient and flexible solution that solves most of the communication problems without adding a new layer of complexity to the developed application.