﻿using hxf_service;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HXF.WebServices.TestClient
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            TestInterfaceClient client = new TestInterfaceClient();
            JObject jo = client.GetStudents();
            Student[] students = JsonConvert.DeserializeObject<Student[]>(jo["Value"].ToString());
            dataGridView1.DataSource = students;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = null;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SimpleInterfaceClient client = new SimpleInterfaceClient();
            lblPingResult.Text = client.Ping()["Value"].ToString();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            SimpleInterfaceClient client = new SimpleInterfaceClient();
            lblSayHelloResult.Text = client.SayHello(edtName.Text)["Value"].ToString();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            TestInterfaceClient client = new TestInterfaceClient();
            int x = int.Parse(edtX.Text);
            int y = int.Parse(edtY.Text);
            lblSumResult.Text = client.Sum(x, y)["Value"].ToString();
        }
    }
}
