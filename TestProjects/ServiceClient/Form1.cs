﻿using Contracts;
using my_service;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ServiceClient.TestService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ServiceClient
{
    public partial class Form1 : Form
    {
        private Stopwatch sw = new Stopwatch();
        
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            lblStatus.Text = "Invoking...";
            TestServiceClient client = new TestService.TestServiceClient("BasicHttpBinding_ITestService");
            sw.Reset();
            sw.Start();
            client.NoPayLoad();
            sw.Stop();
            lblStatus.Text = "Completed in: " + sw.ElapsedMilliseconds + " ms";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            lblStatus.Text = "Invoking...";
            TestServiceClient client = new TestService.TestServiceClient("BasicHttpBinding_ITestService");
            sw.Reset();
            sw.Start();
            int sum = client.Sum(5, 10);
            sw.Stop();
            lblStatus.Text = "Completed in: " + sw.ElapsedMilliseconds + " ms";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            lblStatus.Text = "Invoking...";
            TestServiceClient client = new TestService.TestServiceClient("BasicHttpBinding_ITestService");
            sw.Reset();
            sw.Start();
            Student[] students = client.GetStudentList(200);
            sw.Stop();
            lblStatus.Text = "Completed in: " + sw.ElapsedMilliseconds + " ms";
        }

        private void button6_Click(object sender, EventArgs e)
        {
            lblStatus.Text = "Invoking...";
            HXFServiceClient client = new HXFServiceClient();
            sw.Reset();
            sw.Start();
            client.NoPayLoad();
            sw.Stop();
            lblStatus.Text = "Completed in: " + sw.ElapsedMilliseconds + " ms";
        }

        private void button5_Click(object sender, EventArgs e)
        {
            lblStatus.Text = "Invoking...";
            HXFServiceClient client = new HXFServiceClient();
            sw.Reset();
            sw.Start();
            JObject jo = client.Sum(5, 10);
            sw.Stop();
            lblStatus.Text = "Completed in: " + sw.ElapsedMilliseconds + " ms";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            lblStatus.Text = "Invoking...";
            HXFServiceClient client = new HXFServiceClient();
            sw.Reset();
            sw.Start();
            JObject jo = client.GetStudentList(200);
            JArray ja = (JArray)jo["Value"];
            List<Student> students = JsonConvert.DeserializeObject<List<Student>>(ja.ToString());
            int x = students.Count;
            sw.Stop();
            lblStatus.Text = "Completed in: " + sw.ElapsedMilliseconds + " ms";
        }

        
    }
}
