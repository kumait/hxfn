﻿using Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace HXF.Testing
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "TestService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select TestService.svc or TestService.svc.cs at the Solution Explorer and start debugging.
    public class TestService : ITestService
    {
        public int Sum(int x, int y)
        {
            int z = x + y;
            return z;
        }

        public void NoPayLoad()
        {
            // do nothing
        }

        private Student getRandomStudent()
        {
            Student student = new Student();
            Random r = new Random();
            student.Name = Guid.NewGuid().ToString();
            student.Section = Guid.NewGuid().ToString();
            student.Age = r.Next(8, 40);
            student.Grade = r.Next(0, 101);
            return student;
        }

        public List<Student> GetStudentList(int count)
        {
            List<Student> students = new List<Student>();
            for (int i = 0; i < count; i++)
            {
                students.Add(getRandomStudent());
            }
            return students;
        }
    }
}
