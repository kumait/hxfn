﻿using HXF.WebServices;
using HXF.WebServices.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HXF.Testing
{
    /// <summary>
    /// Summary description for Handler
    /// </summary>
    public class Handler : WebHandler
    {
        public Handler()
        {
            ServiceConfiguration sconf = new ServiceConfiguration("my_service", "service desc");
            sconf.AddInterfaceConfig("interface1", "intr desc", typeof(ITestService), typeof(TestService));
            runtimeConfiguration.ServiceConfiguration = sconf;
        }
        
    }
}