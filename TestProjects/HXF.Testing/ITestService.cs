﻿using Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace HXF.Testing
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ITestService" in both code and config file together.
    [ServiceContract]
    public interface ITestService
    {
        [OperationContract]
        void NoPayLoad();
        
        [OperationContract]
        int Sum(int x, int y);

        [OperationContract]
        List<Student> GetStudentList(int count);
    }
}
