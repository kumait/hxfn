﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebServicesPerformanceTester.TestService;

namespace WebServicesPerformanceTester
{
    class Program
    {
        private const int REPEAT = 100;

        static void Main(string[] args)
        {
            WcfTests wcfTests = new WcfTests();
            HxfTests hxfTests = new HxfTests();
            PerformanceTester tester = new PerformanceTester();
            long time = 0;

            
            Console.WriteLine("WCF Tests");
            Console.WriteLine("=========");
            
            Console.Write("NoPayLoad: ");
            time = tester.TestMethod(wcfTests.TestNoPayLoad, REPEAT);
            Console.WriteLine(time + " ms");

            Console.Write("SimpleSum: ");
            time = tester.TestMethod(wcfTests.TestSimpleSum, REPEAT);
            Console.WriteLine(time + " ms");

            Console.Write("PayLoad: ");
            time = tester.TestMethod(wcfTests.TestPayLoad, REPEAT);
            Console.WriteLine(time + " ms");

            Console.WriteLine();

            Console.WriteLine("HXF Tests");
            Console.WriteLine("=========");

            Console.Write("NoPayLoad: ");
            time = tester.TestMethod(hxfTests.TestNoPayLoad, REPEAT);
            Console.WriteLine(time + " ms");

            Console.Write("SimpleSum: ");
            time = tester.TestMethod(hxfTests.TestSimpleSum, REPEAT);
            Console.WriteLine(time + " ms");

            Console.Write("PayLoad: ");
            time = tester.TestMethod(hxfTests.TestPayLoad, REPEAT);
            Console.WriteLine(time + " ms");
            
        }
    }
}
