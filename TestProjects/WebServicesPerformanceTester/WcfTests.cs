﻿using Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebServicesPerformanceTester.TestService;

namespace WebServicesPerformanceTester
{
    public class WcfTests
    {
        private TestServiceClient client;

        public WcfTests()
        {
            client = new TestService.TestServiceClient("BasicHttpBinding_ITestService");
        }

        public void TestNoPayLoad()
        {
            
            client.NoPayLoad();
        }

        public void TestSimpleSum()
        {
            int sum = client.Sum(5, 10);
        }

        public void TestPayLoad()
        {
            Student[] students = client.GetStudentList(200);
            int count = students.Count();
        }
    }
}
