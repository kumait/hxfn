﻿using Contracts;
using my_service;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebServicesPerformanceTester
{
    public class HxfTests
    {
        HXFServiceClient client;

        public HxfTests()
        {
            client = new HXFServiceClient();
        }
        
        public void TestNoPayLoad()
        {
            client.NoPayLoad();
        }

        public void TestSimpleSum()
        {
            JObject jo = client.Sum(5, 10);
        }

        public void TestPayLoad()
        {
            JObject jo = client.GetStudentList(200);
            JArray ja = (JArray)jo["Value"];
            Student[] students = JsonConvert.DeserializeObject<Student[]>(ja.ToString());
            int count = students.Count();
        }
    }
}
