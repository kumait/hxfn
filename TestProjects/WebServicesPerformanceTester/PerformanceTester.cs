﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebServicesPerformanceTester
{
    public class PerformanceTester
    {
        private Stopwatch stopwatch;

        public PerformanceTester()
        {
            this.stopwatch = new Stopwatch();
        }

        public long TestMethod(Action action, int repeat)
        {
            long totalTime = 0;
            for (int i = 0; i < repeat; i++)
            {
                stopwatch.Reset();
                stopwatch.Start();
                action();
                totalTime += stopwatch.ElapsedMilliseconds;
            }

            return totalTime / repeat;  
        }
    }
}
