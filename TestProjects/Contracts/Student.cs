﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Contracts
{
    public class Student
    {
        public string Name { get; set; }
        public string Section { get; set; }
        public int Age { get; set; }
        public int Grade { get; set; }
    }
}