﻿using HXF.WebServices.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HXF.WebServices.TestServer
{
    public class Handler : WebHandler
    {
        public Handler()
        {
            InterfaceConfiguration iconf1 = new InterfaceConfiguration("SimpleInterface", "Simple Interface",
                new RuntimeInfo(typeof(ISimpleInterface), typeof(SimpleInterfaceImpl)));

            InterfaceConfiguration iconf2 = new InterfaceConfiguration("TestInterface", "Test Interface",
                new RuntimeInfo(typeof(ITestInterface), typeof(TestInterfaceImpl)));
            
            ServiceConfiguration sconf = new ServiceConfiguration("hxf_service", "HXF Service");
            sconf.InterfaceConfigs.Add(iconf1);
            sconf.InterfaceConfigs.Add(iconf2);
            runtimeConfiguration.ServiceConfiguration = sconf;
        }
    }
}