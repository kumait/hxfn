function TestInterface() { self = this; }

TestInterface.prototype = {
    urlString: 'http://localhost/HXF.WebServices.TestServer/Handler.ashx',
    
    Sum: function(x, y, successFunction, failFunction) {
        var data = { 'interface': 'TestInterface', 'method': 'Sum', 'parameters': {'x': x, 'y': y} }
        
        var jsonData = dojo.toJson(data);
        var xhrArgs = {
            url: self.urlString,
            handleAs: 'json',
            postData: jsonData,
            load: successFunction,
            error: failFunction };
        var deferred = dojo.xhrPost(xhrArgs);
        
    },
    
    GetStudents: function(successFunction, failFunction) {
        var data = { 'interface': 'TestInterface', 'method': 'GetStudents', 'parameters': {} }
        
        var jsonData = dojo.toJson(data);
        var xhrArgs = {
            url: self.urlString,
            handleAs: 'json',
            postData: jsonData,
            load: successFunction,
            error: failFunction };
        var deferred = dojo.xhrPost(xhrArgs);
        
    }
    
}
