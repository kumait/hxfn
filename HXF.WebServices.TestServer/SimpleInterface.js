function SimpleInterface() { self = this; }

SimpleInterface.prototype = {
    urlString: 'http://localhost/HXF.WebServices.TestServer/Handler.ashx',
    
    Ping: function(successFunction, failFunction) {
        var data = { 'interface': 'SimpleInterface', 'method': 'Ping', 'parameters': {} }
        
        var jsonData = dojo.toJson(data);
        var xhrArgs = {
            url: self.urlString,
            handleAs: 'json',
            postData: jsonData,
            load: successFunction,
            error: failFunction };
        var deferred = dojo.xhrPost(xhrArgs);
        
    },
    
    SayHello: function(name, successFunction, failFunction) {
        var data = { 'interface': 'SimpleInterface', 'method': 'SayHello', 'parameters': {'name': name} }
        
        var jsonData = dojo.toJson(data);
        var xhrArgs = {
            url: self.urlString,
            handleAs: 'json',
            postData: jsonData,
            load: successFunction,
            error: failFunction };
        var deferred = dojo.xhrPost(xhrArgs);
        
    }
    
}
